package com.szkolenie.kurier;

public class Package {
    private Status status = Status.REGISTERED;
    private int number;
    private int region;

    public Package(int number) {
        this.number = number;
    }

    public Package(Status status, int number) {
        this.status = status;
        this.number = number;
    }

    public Package(Status status, int number, int region) {
        this.status = status;
        this.number = number;
        this.region = region;
    }

    public int getRegion() {
        return region;
    }

    public void setRegion(int region) {
        this.region = region;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Package{" +
                "status=" + status +
                ", number=" + number +
                ", region=" + region +
                '}';
    }
}
