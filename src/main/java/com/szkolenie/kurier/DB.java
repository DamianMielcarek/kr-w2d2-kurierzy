package com.szkolenie.kurier;

import java.util.LinkedList;
import java.util.List;

public class DB {
    public static List<Package> getPackages() {
        List<Package> packages = new LinkedList<>();

        packages.add(new Package(19));
        packages.add(new Package(Status.DELIVERED, 20));
        packages.add(new Package(30));
        packages.add(new Package(Status.POSTED, 50, 3));
        packages.add(new Package(55));
        packages.add(new Package(Status.POSTED, 59, 1));
        packages.add(new Package(Status.POSTED, 60, 2));
        packages.add(new Package(Status.DELIVERED, 61));
        packages.add(new Package(Status.DELIVERED, 62, 1));

        return packages;
    }

    public static List<Courier> getCouriers() {
        List<Courier> couriers = new LinkedList<>();

        couriers.add(new Courier("Jan Nowak", 1));
        couriers.add(new Courier("Kamil Kowalski", 2));
        couriers.add(new Courier("Dominik Krasinski", 3));

        return couriers;
    }
}
