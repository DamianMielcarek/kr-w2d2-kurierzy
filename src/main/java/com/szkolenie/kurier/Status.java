package com.szkolenie.kurier;

public enum Status {
    REGISTERED, POSTED, DELIVERED;
}
